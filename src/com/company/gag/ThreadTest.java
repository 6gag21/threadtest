package com.company.gag;

public class ThreadTest {
    String name;
    int executionTime = 0;
    int totalTime;
    public ThreadTest(String name, int totalTime){
            this.name = name;
            this.totalTime = totalTime;
    }

    public int subtractTime(int time) {

        if (time + executionTime > totalTime) {
            int result = time + executionTime - totalTime;
            executionTime = totalTime;
            return result;
        } else {
            executionTime += time;
            return 0;
        }
    }

    public void printTime(){
        System.out.println(this.name + " " + executionTime);
    }

}
