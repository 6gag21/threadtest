package com.company.gag;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Test {
    static List<ThreadTest> list = new ArrayList<>();
    static int firstTime;
    static int secondTime;
    static int thirdTime;
    static int percent;
    static int fullTime;
    static int providedTimeByPercentFromFullTime;
    static int providedTimeByPercentFromBiggestTime;
    static int providedTimeByPercentFromSmallestTime;
    static int providedTimeByPercentFromMiddleTime;

    static {
        firstTime = 100;
        secondTime = 200;
        thirdTime = 300;
        percent = 5;
        fullTime = firstTime + secondTime + thirdTime;
        providedTimeByPercentFromFullTime = fullTime * percent / 100;
        providedTimeByPercentFromBiggestTime = thirdTime * percent / 100;
        providedTimeByPercentFromMiddleTime = thirdTime * percent / 100;
        providedTimeByPercentFromSmallestTime = firstTime * percent / 100;
    }


    public static void main(String[] args) {

        list.add(new ThreadTest("First", 100));
        list.add(new ThreadTest("Second", 200));
        list.add(new ThreadTest("Third", 300));

//        simpleFirst();
//        simpleSecond();
//        simpleThird();
        simpleFourth();

    }

    // Поочередное предоставление определенного процента времени от общего количества времени всем тредам
    private static void simpleFirst() {
        for (int i = 0; i < list.size(); i++) {
            list.get(i).printTime();
            int result = list.get(i).subtractTime(providedTimeByPercentFromFullTime);
            if (result == 0) {
                fullTime -= providedTimeByPercentFromFullTime;
            } else {
                fullTime -= providedTimeByPercentFromFullTime - result;
                list.remove(i);
            }

            if (i == list.size() - 1) {
                i = -1;
            }
        }
        System.out.println("Full Time : " + fullTime);
    }

    //Поочередное предоставление определенного процента времени от временем выполнения определенного треда
    private static void simpleSecond() {
        for (int i = 0; i < list.size(); i++) {
            list.get(i).printTime();
            int result = list.get(i).subtractTime(providedTimeByPercentFromBiggestTime);
            if (result == 0) {
                fullTime -= providedTimeByPercentFromBiggestTime;
            } else {
                fullTime -= providedTimeByPercentFromBiggestTime - result;
                list.remove(i);
            }

            if (i == list.size() - 1) {
                i = -1;
            }
        }
        System.out.println("Full Time : " + fullTime);
    }

    //сортируем список и сперва выполняем тред с наименьшим временем выполнения и т.д. по возрастанию
    private static void simpleThird() {

        list.sort(new Comparator<ThreadTest>() {
            @Override
            public int compare(ThreadTest o1, ThreadTest o2) {
                return Integer.compare(o1.totalTime, o2.totalTime);
            }
        });

        int arrayIndex = 0;

        while (true) {
            list.get(arrayIndex).printTime();
            int result = list.get(arrayIndex).subtractTime(5);
            if (result == 0) {
                fullTime -= 5;
            } else {
                fullTime -= 5 - result;
                list.remove(arrayIndex);
            }
            if (list.isEmpty()) break;
        }


        System.out.println("Full Time : " + fullTime);
    }

    //Поочередное предоставление определенного процента времени,
    // вычисляемого отдельно для каждого треда в зависимости от времени его выполнения
    private static void simpleFourth() {
        int providedTime;
        for (int i = 0; i < list.size(); i++) {
            providedTime = list.get(i).totalTime * percent / 100;
            list.get(i).printTime();
            int result = list.get(i).subtractTime(providedTime);
            if (result == 0) {
                fullTime -= providedTime;
            } else {
                fullTime -= providedTime - result;
                list.remove(i);
            }

            if (i == list.size() - 1) {
                i = -1;
            }
        }
    }
}
